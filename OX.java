import java.util.Scanner;

public class OX {
	static char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };

	public static void main(String[] args) {
		showWelcome();
		while (true) {
			showTable();
			showTurn();
			input();
			if (checkWin()) {
				break;
			}
			switchPlayer();
		}
		showWin();
	}

	private static void showWelcome() {
		System.out.println("Welcome to XO Game");
	}

	private static void showTable() {
		for (int i = 0; i < table.length; i++) {
			System.out.print(i+1);
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private static void showTurn() {
		// TODO Auto-generated method stub

	}

	private static void input() {
		// TODO Auto-generated method stub

	}

	private static boolean checkWin() {
		return true;

	}

	private static void switchPlayer() {
		// TODO Auto-generated method stub

	}

	private static void showWin() {
		// TODO Auto-generated method stub

	}
}
